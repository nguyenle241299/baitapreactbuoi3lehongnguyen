import React, { Component } from 'react'

export default class 
 extends Component {
  render() {
    let { image, name, price } = this.props.data;
    return (
      <div className="col-3 p-1">
        <div className="card text-left h-100">
          <img className="card-img-top" src={image} alt />
          <div className="card-body">
            <h5 className="card-title">{name}</h5>
            <h2 className="card-title">{price} $</h2>
          </div>
          <div>
            <button
              onClick={() => {
                this.props.handleClick(this.props.data);
              }}
              className="btn btn-secondary"
            >
              Xem chi tiết
            </button>
            <button
              onClick={() => {
                this.props.handleBuyShoe(this.props.data);
              }}
              className="btn btn-danger"
            >
              Chọn mua
            </button>
          </div>
        </div>
      </div>
    );
    }
}
