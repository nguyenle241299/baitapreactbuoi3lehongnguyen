import React, { Component } from 'react'
import { dataShoe } from './dataShoe';
import Detail from './Detail';
import CartShoe from './CartShoe';
import ListShoe from './ListShoe';

export default class ShoeShop extends Component {
    state = {
        shoeArr: dataShoe,
        detail: dataShoe[0],
        cart: []
    }

    handleChangeDetail = (shoe) => {
        this.setState({
            detail: shoe,
        })
    }

    handleAddToCart = (shoe) => {
      let cloneCart = [...this.state.cart];

      let index = this.state.cart.findIndex((item) => {
        return item.id == shoe.id
      })

      if( index == -1) {
        // Chưa có sản phẩm =>> thêm mới
        let cartItem = {...shoe, number: 1}
        cloneCart.push(cartItem);
      } else {
        // cloneCart.push(cartItem)++;
        cloneCart[index].number++;
      }

      this.setState ({ cart: cloneCart})
    } 

  render() {
    return (
      <div className='container'>
        <CartShoe cartShoe = {this.state.cart}/>
        {/* <div className='row'>{this.renderListShoe()}</div> */}
        <ListShoe
         shoeArr = {this.state.shoeArr}
         handleAddToCart = {this.handleAddToCart}
         handleChangeDetail = {this.handleChangeDetail}
         />
        <Detail detailShoe = {this.state.detail}/>
      </div>
    )
  }
}
