import React, { Component } from 'react'

export default class Detail extends Component {
  render() {
      let {image, desc, name, price} = this.props.detailShoe
    return (
      <div className='bg-secondary row w-50 mt-5 mb-5'>
        <img className='col-6' src={image} alt="" />
        <div className='col-6 d-flex flex-column align-item-center pt-5 pb-5'>
            <h4 className='text-white '>Name: {name}</h4>
            <h5 className='text-white '>Price: {price}</h5>
            <h6 className='text-white '>Description: {desc}</h6>
        </div>
      </div>
    )
  }
}
