import React, { Component } from 'react'

export default class CartShoe extends Component {

    renderTbody = () => {
        return this.props.cartShoe.map((item) => {
            return <tr>
                <td>{item.id}</td>
                <td>{item.name}</td>
                <td>
                  <img style={{width: "50px"}} src={item.image} alt="" />
                </td>
                <td>

                  <span>{item.number}</span>
                </td>
                <td>{item.price * item.number} $</td>
            </tr>
        })
    }
  render() {
    return (
      <table className='table'>
        <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Image</th>
                <th>Number</th>
                <th>Price</th>
            </tr>
        </thead>
        <tbody>
            {this.renderTbody()}
        </tbody>
      </table>
    )
  }
}
